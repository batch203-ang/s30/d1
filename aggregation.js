db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
])

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: 1}}
])

db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id:"$origin", kinds:{$sum: 1}}}
])

db.fruits.aggregate([
	{$match:{color:"Yellow"}},
	{$count: "Yellow Fruits"}
])

db.fruits.aggregate([
	{$match:{color:"Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock:{$avg: "$stock"}}}
])

db.fruits.aggregate([
	{$match:{color:"Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock:{$min: "$stock"}}}
])

db.fruits.aggregate([
	{$match:{color:"Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock:{$max: "$stock"}}}
])